import org.apache.spark.rdd
import org.apache.spark.sql

/*
 * Date class definition
 */
case class Date() {
	var day:Int = 0
	var month:Int = 0
	var year:Int = 0

	/*
	 * Constructor.
	 */
	def this( d:Int, m:Int, y:Int) {
		this
		this.day = d
		this.month = m
		this.year = y
	}

	/*
	 * Constructor. It will parse the string before
	 * --> the string should be like "month/day/year <time>"
	 * --> the <time> will be dropped
	 */
	def this( date:String ) {
		this
		var elems = date.split(" ")(0).split("/")
		this.day = elems(1).toInt
		this.month = elems(0).toInt
		this.year = elems(2).toInt
	}

	/*
	 * @returns string representing the date according to the format "MM/dd/yy"
         */
	def get : String =  {
		var s:String = ""
		if ( month < 10) s += "0"+month else s += month
		s+="/"
		if ( day < 10) s += "0"+day else s += day
		s+="/"
		if ( year < 10) s += "0"+year else s += year
		return s
	}

	/*
         * @param date
         * @returns true if 'date' is equals to this object or false otherwise 
         */
	def equals( date:Date ): Boolean = {
		if ( day==date.day && month==date.month && year==date.year )
			return true
		else
			return false 
	}
}

/*
 * Crime class definition
 */
case class Crime(date:String, district:String, crimedescr:String)

  
/* 
 * importing crimes.csv file and mapping to partitions
 *	-> the first line of the file should be dropped because its the header of the file
 */
val file = sc.textFile("/home/hadoop/ece-hadoop-spark-assignment/csv/crimes.csv").mapPartitionsWithIndex { 
	(id, iter) => if( id==0 ) iter.drop(1) else iter 
}


/*
 * Creating Crime objects for each entry on 'file'
 *	-> column 0 = date
 *	-> column 2 = district
 *	-> column 5 = crime description
 */
val crimes = file.map ( line => {
	val columns = line.split(",")
	Crime( new Date(columns(0)).get , columns(2) , columns(5) )
})

/********************************************************************************************************/
//						RDD							//
/********************************************************************************************************/

/*
 * QUERY 1
 * 
 * 	What is the crime that happens the most in Sacramento ?
 *
 */
// (key,value) => (crime description, sum of occurrences)
// @map each crime description with a value of 1
// @reduce by key (crimedescr), summing all the occurrences
// @note _+_ is a shorthand for a function of two arguments that returns their sum
val query1 = crimes.map( crime => (crime.crimedescr,1)).reduceByKey(_+_)

// Sort the results based on the second argument of paris, in descending order and take the first
//  and parallelize the array of results
val query1rdd = sc.parallelize(query1.sortBy( tuple => tuple._2, false).take(1))


/*
 * QUERY 2
 * 
 * 	Give the 3 days with the highest crime count
 *
 */
// (key,value) => (date, count of occurrences)
// @map each date with a value of 1
// @reduce by date, summing all the occurrences
val query2 = crimes.map ( crime => (crime.date,1)).reduceByKey(_+_)

// Sort the results in descending order (false), using the second argument of the pair
//  and take the first 3 tuples. finally, parallelize the array
val query2rdd = sc.parallelize(query2.sortBy( tuple => tuple._2 , false ).take(3))


/*
 * QUERY 3
 * 
 * 	Calculate the average of each crime per day
 *
 */
// like query1 but now, the count should be devide by the number of day in january (31)
// ---> in crimes.csv only exist records for january of 2006
val query3 = crimes.map( crime => (crime.crimedescr,1)).reduceByKey(_+_)
val query3rdd = query3.map( x => (x._1, (x._2)/31.0))


/********************************************************************************************************/
//						DATA FRAME						//
/********************************************************************************************************/

val df = crimes.toDF

/*
 * QUERY 1
 */
// select the values of 'crimedescr' and group all equal values, counting the number of equal occurrences
// sort the results based on the count in descending order. keep only the first row.
val query1df = df.select("crimedescr").groupBy("crimedescr").count.sort($"count".desc).limit(1)

/*
 * QUERY 2
 */
// select the values of 'date' and goup all equal values, counting the number of equal occurrences
// sort the results by this count in descending order and keep the first 3 rows.
val query2df = df.select("date").groupBy("date").count.sort($"count".desc).limit(3)

/*
 * QUERY 3
 */
// select the values of 'crimedescr' and group all equal values, counting the number of equals occurences
// and finally select bothe columns, dividing the second (the count) by 31 (the number of day in January)
val query3df = df.select("crimedescr").groupBy("crimedescr").count.select($"crimedescr",$"count"/31.0)

/*
 * QUERY 4
 *
 *	Average of Crimes per day per district
 *
 */
// select the values of 'district' and 'crimedescr'. Then group all the results based on both columns and count
// the number of equal occurrences. Finally select all the columns, dividing the count by 31 (number of day in January)
// the results are then sorted by 'district' in ascending order
val query4df = df.select("district","crimedescr").groupBy("district","crimedescr").count.select($"district",$"crimedescr",$"count"/31.0).sort($"district".asc)


/********************************************************************************************************/
//						EXPORT RESULTS						//
/********************************************************************************************************/

/*
 * Saving RDD results
 */
query1rdd.map(tuple => "%s,%s".format(tuple._1, tuple._2)).coalesce(1).
	saveAsTextFile("/home/hadoop/ece-hadoop-spark-assignment/results/rdd/query1")

query2rdd.map(tuple => "%s,%s".format(tuple._1, tuple._2)).coalesce(1).
	saveAsTextFile("/home/hadoop/ece-hadoop-spark-assignment/results/rdd/query2")

query3rdd.map(tuple => "%s,%s".format(tuple._1, tuple._2)).coalesce(1).
	saveAsTextFile("/home/hadoop/ece-hadoop-spark-assignment/results/rdd/query3")

/*
 * Saving DataFrame results
 */
query1df.repartition(1).write.format("csv").save("/home/hadoop/ece-hadoop-spark-assignment/results/df/query1")
query2df.repartition(1).write.format("csv").save("/home/hadoop/ece-hadoop-spark-assignment/results/df/query2")
query3df.repartition(1).write.format("csv").save("/home/hadoop/ece-hadoop-spark-assignment/results/df/query3")

query4df.repartition(1).write.format("csv").save("/home/hadoop/ece-hadoop-spark-assignment/results/df/query4")
